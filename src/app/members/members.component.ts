import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { moveIn, fallIn, moveInLeft } from '../router.animations';
import { AngularFire, AuthProviders, AuthMethods,FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class MembersComponent implements OnInit {
  name: any;
  state: string = '';
  items: FirebaseListObservable<any>;
  msgVal: string = '';


  constructor(public af: AngularFire,private router: Router) {

    this.af.auth.subscribe(auth => {
      if(auth) {
        this.name = auth;
      }

    });


        this.items = af.database.list('/messages', {
      query: {
        limitToLast: 5
      }
    });

        this.af.auth.subscribe(auth => { 
      if(auth) {
        this.name = auth;
      }
    });

  }
  

  logout() {
     this.af.auth.logout();
     console.log('logged out');
     this.router.navigateByUrl('/login');
  }

  chatSend(theirMessage: string) {
      this.items.push({ message: theirMessage, name});
      this.msgVal = '';
  }


  ngOnInit() {
  }

}
